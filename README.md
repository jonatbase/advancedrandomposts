# Advanced Random Posts with Thumbnails

I was using yakuphan's excellent Advanced Random Posts plugin. It has support for resizing images from posts but this often distorts the image. So I added the timthumb plugin to resize and crop the images down. A simple addition/combo, most of the work was from the original plugin and from timthumb - but, you know, *someone* might find it useful. :)

Based on yakuphan's [Advanced Random Posts Plugin](http://wordpress.org/extend/plugins/advanced-random-posts/).

## Extras
Uses [TimThumb.php](http://code.google.com/p/timthumb/).

## History

#### 0.1
- Initial release

## License

GNU licensed

Copyright (C) 2012 Jonathan Ginn, http://jonginn.com & Copyright (C) 2009 Jakub Govler, http://www.yakupgovler.com/